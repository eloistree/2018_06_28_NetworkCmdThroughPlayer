﻿using UnityEngine;
using System.Collections;

public class ExampleOfUse : MonoBehaviour
{
    private int rand;                   //Random number for title
    private bool changeTitle = false;   //If title has changed
    private float timer;                
    private bool hidden = false;        //if window is hidden

	void OnGUI()
    {
#if UNITY_EDITOR
        //Alert in editor mode
        GUI.color = new Color(1f, 0, 0, Mathf.Abs(Mathf.Cos(Time.time)));
        GUI.Label(new Rect(200, 0, Screen.width, Screen.height), "You are in edit mode, WindowTools only works in Windows Stand Alone" + WindowTools.GetActiveWindowTitle());
        GUI.color = Color.white;
#endif
        //Show the title of this aplication
        GUI.Label(new Rect(0, 0, Screen.width, Screen.height),"Window Title: " + WindowTools.GetActiveWindowTitle());

        //Set window Always on top
        if (GUI.Button(new Rect(10, 30, 210, 100), "Top Most\nPlaces the window\nabove all non-topmost windows."))
        {
            WindowTools.MakeTopMost();
        }

        //Disable Always on top
        if (GUI.Button(new Rect(230, 30, 210, 100), "No Top Most\nPlaces the window\nabove all\nnon-topmost windows\n(behind all topmost windows)."))
        {
            WindowTools.MakeNoTopMost();
        }

        //Send window to bottom
        if (GUI.Button(new Rect(450, 30, 210, 100), "Bottom\nPlaces the window\nat the bottom of the Z order."))
        {
            WindowTools.MakeBottom();
        }

        //Set window on top
        if (GUI.Button(new Rect(670, 30, 210, 100), "Top\nPlaces the window\nat the top of the Z order."))
        {
            WindowTools.MakeTop();
        }

        //Generate new random title
        if (changeTitle)
        {
            rand = Random.Range(0, 1000);
            changeTitle = false;
        }
        
        //Change title with a random title
        if (GUI.Button(new Rect(10, 150, 210, 100), "Change title to: My new Title" + rand.ToString("000")))
        {
            WindowTools.SetWindowTitle("My new Title"+rand.ToString("000"));
            changeTitle = true;
        }

        //Hide window from screen and taskbar and run in background
        if (GUI.Button(new Rect(230, 150, 210, 100), "Hide window for 5 seconds\n(This action activate\nRun in background\nand restore when back)"))
        {
            hidden = true;
            timer = Time.time;
            WindowTools.HideWindow();
        }

        //5 seconds later of hide window, show it
        if (hidden && (Time.time-timer)>=5f)
        {
            WindowTools.ShowWindow();
        }

        //Make window half transparent
        if (GUI.Button(new Rect(450, 150, 210, 100), "Set opacitty to 50%\n(Not work in editor)"))
        {
            WindowTools.SetAlphaWindow(0.5f);
        }

        //Make window solid
        if (GUI.Button(new Rect(670, 150, 210, 100), "Set opacitty to 100%\n(Not work in editor)"))
        {
            WindowTools.SetAlphaWindow(1f);
        }

        //Resize window
        if (GUI.Button(new Rect(10, 270, 210, 100), "Change Size to 1024 x 600"))
        {
            WindowTools.SetWindowSize(1024, 600);
        }

        //Resize window
        if (GUI.Button(new Rect(230, 270, 210, 100), "Change Size to 800 x 600"))
        {
            WindowTools.SetWindowSize(800, 600);
        }

        //Move window to 0,0
        if (GUI.Button(new Rect(450, 270, 210, 100), "Move to 0,0"))
        {
            WindowTools.SetWindowPosition(0, 0);
        }

        //Move window to 300,400
        if (GUI.Button(new Rect(670, 270, 210, 100), "Move to 300,400"))
        {
            WindowTools.SetWindowPosition(300, 400);
        }

        //Hide Borders
        if (GUI.Button(new Rect(10, 420, 210, 100), "Hide Borders"))
        {
            BorderlessWindow.NoBorder();
        }

        //Show Borders
        if (GUI.Button(new Rect(230, 420, 210, 100), "Show Borders"))
        {
            BorderlessWindow.ShowBorder();
        }
        
    }
}
