﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

[RequireComponent(typeof(NetworkObjectSceneId))]
public class ChangeColor : MonoBehaviour {

    public NetworkObjectSceneId m_id;

    public void OnMouseDown()
    {
        Color c = UnityRandom.RandomColor();
        ChangeWithColor(c);
        CmdChangeColorOf.singleton.ChangeColor(m_id.GetId(),c);
    }

    public void Reset()
    {
        m_id = GetComponent<NetworkObjectSceneId>();
    }


    public void ChangeWithColor(Color color) {
        GetComponent<Renderer>().material.color = color;

    }
}
