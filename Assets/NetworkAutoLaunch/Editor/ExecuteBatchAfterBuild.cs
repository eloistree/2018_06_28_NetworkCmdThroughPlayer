﻿// C# example:
using UnityEngine;
using UnityEditor;
using UnityEditor.Callbacks;
using System.Diagnostics;

using UnityEditor.Build;
public class ExecuteBatchAfterBuild : IPreprocessBuild
{
  
    [PostProcessBuildAttribute(1)]
    public static void OnPostprocessBuild(BuildTarget target, string pathToBuiltProject)
    {

        UnityEngine.Debug.Log("Hello :)");
        string path = Application.dataPath + "/NetworkAutoLaunch/";
        //ExecuteCommand(path + "KillAll.bat");
        ExecuteCommand(path + "AutoLaunch.bat");
    }

    
    [MenuItem("Window/Auto Launch/Kill %#&k")]
    public static void KillAll()
    {
        string path = Application.dataPath + "/NetworkAutoLaunch/";
        ExecuteCommand(path + "KillAll.bat");

    }
    public int callbackOrder { get { return 0; } }
    public void OnPreprocessBuild(BuildTarget target, string path)
    {
        KillAll();
    }

static void ExecuteCommand(string path)
    {
        var processInfo = new ProcessStartInfo(path);
        //processInfo.CreateNoWindow = true;
        //processInfo.UseShellExecute = false;
        //processInfo.RedirectStandardError = true;
        //processInfo.RedirectStandardOutput = true;

        var process = Process.Start(processInfo);

       // process.WaitForExit();
        
        process.Close();
    }

}